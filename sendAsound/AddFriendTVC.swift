//
//  AddFriendTVC.swift
//  sendAsound
//
//  Created by IT-Högskolan on 29/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import UIKit
import Parse


class AddFriendTVC: UITableViewController {
    
    var userList = [PFObject]()
    var fileteredUserList = [PFObject]()
    var pendingListUsers = [PFUser]()
    var requestsListUsers = [PFUser]()
    var friendsList = [PFUser]()
    var selectedUser: PFUser?
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        
        //self.definesPresentationContext = true
        
        tableView.tableHeaderView = searchController.searchBar
        
        if self.userList.isEmpty { loadUserList() }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        prefersStatusBarHidden()
        
        loadFriendsList()
        loadPendingListUsers()
        
        searchController.searchBar.hidden = false
        tableView.reloadData()
        
        let selection = tableView.indexPathForSelectedRow
        
        if (selection != nil) {        
            self.tableView.deselectRowAtIndexPath(selection!, animated: true)
        }
        
      //  cleanUp()
    }
    
    func cleanUp() {
        var index = 0
        
        print("CLEAN UP CALLED")

        for friend in pendingListUsers {
            if friendsList.contains(friend) {
                pendingListUsers.removeAtIndex(index)
                removeUserFromPendingList(friend)
                index += 1
                print("Pend List users count: ", pendingListUsers.count)
            }
        }
        for friend in pendingListUsers {
            if requestsListUsers.contains(friend) {
                pendingListUsers.removeAtIndex(index)
                removeUserFromPendingList(friend)
                index += 1
                print("Pend List users count: ", pendingListUsers.count)
            }
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        print("AddFriend viewDidDisappear called")
        searchController.searchBar.hidden = true
    
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }

    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Futura", size: 20)!
        header.textLabel?.textColor = UIColor.yellowColor()
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 && pendingListUsers.count > 0 {
            
            return "Friends pending:"
        } else {
            return "Request a friend: "
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        if pendingListUsers.count > 0 { return 2 } else { return 1 }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var sectionRowCount = 0
        
        if pendingListUsers.count > 0 {
            // There are friend requests
            if section == 0 {
                print("section 0, pending count: ", pendingListUsers.count)
                sectionRowCount = pendingListUsers.count
            } else if section == 1 {
                print("section 1, fileteredUserList count: ", fileteredUserList.count)
                sectionRowCount = fileteredUserList.count
            }
        } else {
            // There are no friend requests
            if section == 0 {
                print("section 0, no requests,  fileteredUserList count: ", fileteredUserList.count)
                sectionRowCount = fileteredUserList.count
            }
        }
        return sectionRowCount
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
        
        setUpCell(cell)
        
        print("pendingListUsers count: ", pendingListUsers.count)
        
        if indexPath.section == 0 && pendingListUsers.count > 0  {
            
            cell.imageView!.image = UIImage(named: "FBNoPicture50x50")
            
            makePendingListCells(cell, indexPath: indexPath)
            
            cell.accessoryView?.hidden = true

        } else if fileteredUserList.count > 0 {
        
            cell.imageView!.image = UIImage(named: "FBNoPicture50x50")
            createPlusAccessoryView(cell)
            
            var filteredUser = PFUser()
            filteredUser = getFilteredUser(indexPath.row)
            
            let firstName = filteredUser["first_name"]
            let lastName  = filteredUser["last_name"]
            
            cell.textLabel?.text = "\(firstName) \(lastName)"
            
            if filteredUser == PFUser.currentUser() {
                cell.detailTextLabel?.text = "This is you!"
                cell.accessoryView?.hidden = true
            }
            if pendingListUsers.contains(filteredUser) {
                cell.detailTextLabel?.text = "Friend is pending"
                cell.accessoryView?.hidden = true
            }
            
            if friendsList.contains(filteredUser) {
                cell.detailTextLabel?.text = "Already a friend"
                cell.accessoryView?.hidden = true
            }
            
            if requestsListUsers.contains(filteredUser) {
                cell.detailTextLabel?.text = "Already requested you"
                cell.accessoryView?.hidden = true
            }
            
            if firstName! as! String == "Tilo-Karl" {
            
  //          if firstName.isEqualToString("Tilo-Karl") {
                print("Tilo Developer")
                let str = (cell.detailTextLabel?.text)!
                cell.detailTextLabel?.text = "\(str) 'The Creator'"
            }
            
            if let userPicture = filteredUser["profile_picture"] as? PFFile {
                userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                    if (error == nil) {
                        cell.imageView?.image = UIImage(data:imageData!)
                    }
                }
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        // 3. How to use iAd
        // 4. if i click on search and tab, when I return to controller it is black
        
        if indexPath.section == 0 && pendingListUsers.count > 0 {
            selectedUser = pendingListUsers[indexPath.row] as PFUser
        } else {
            selectedUser = fileteredUserList[indexPath.row] as? PFUser
        }
        
        loadRequestsListUsers()
        
        if requestsListUsers.contains(selectedUser!) {
            print("Friend has already requested you")
            createCrossAccessoryView(cell!)
            return
        }
        
        if selectedUser == PFUser.currentUser() {
            print("Don't add yourself")
            createCrossAccessoryView(cell!)
            return
        }
        
        if pendingListUsers.contains(selectedUser!) {
            print("Friend is already pending")
            createCrossAccessoryView(cell!)
            return
        }
        
        if friendsList.contains(selectedUser!) {
            print("\(selectedUser?.email) is already your friend!")
            createCrossAccessoryView(cell!)
            return
        }
        
        print("row selected: ", cell?.textLabel?.text, "row: ", indexPath.row)
        
        addMeToRequestList((selectedUser!))
        addFriendToPendingList(selectedUser!)
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        tableView.reloadData()
    }
    
    func getFilteredUser(row: Int) -> PFUser {
        var filteredUser =  PFUser()
        if searchController.active && searchController.searchBar.text != "" {
            filteredUser = (fileteredUserList[row] as? PFUser)!
        }
        return filteredUser
    }
    
    func makePendingListCells(cell: UITableViewCell, indexPath: NSIndexPath) {
        
        
        let currentUserPendingList = PFUser.currentUser()!.objectForKey("pendingList")
        
        currentUserPendingList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            
            let user = self.pendingListUsers[indexPath.row]
            
            user.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
                let firstName = user["first_name"]
                let lastName  = user["last_name"]
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    cell.textLabel?.text = "\(firstName) \(lastName)"
                    cell.detailTextLabel?.text =  "Friend is pending"
                    self.loadPicture(user, cell: cell, rowNumber: indexPath.row)
                })
            })
        })
    }
    
    
    func addFriendToPendingList(selectedUser: PFUser) {
        
        let currentUserPendingList = PFUser.currentUser()!.objectForKey("pendingList")
        
        currentUserPendingList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            currentUserPendingList?.addUniqueObject(selectedUser, forKey: "friendsPendingList")
            currentUserPendingList!.saveInBackground()
            print("Pending List Count: ", currentUserPendingList!["friendsPendingList"]!!.count)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.loadPendingListUsers()
            self.tableView.reloadData()
            })
            
        })
        
        pushNotifiction(selectedUser)
    }
    
    func addMeToRequestList(selectedUser: PFUser) {
        let selectedUserRequestList = selectedUser.objectForKey("requestsList") as? PFObject

        selectedUserRequestList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            selectedUserRequestList?.addUniqueObject(PFUser.currentUser()!, forKey: "friendsRequestList")
            selectedUserRequestList!.saveInBackground()
            
        })
    }
    
    func removeUserFromPendingList(selectedUser: PFUser) {
        let list = PFUser.currentUser()!.objectForKey("pendingList") as? PFObject
        
        print("removeUserFromPendingList called")
        list?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            list?.removeObject(selectedUser, forKey: "friendsPendingList")
            list?.saveInBackground()
            
        })
    }
    
    func pushNotifiction(receiver: PFUser) {
        
        let message = " \(PFUser.currentUser()!["first_name"]!) want's to be your friend!"
        
        let data = [
            "alert": message,
            "sound": "default"]
        
        let query: PFQuery = PFInstallation.query()!
        query.whereKey("user", equalTo: receiver)
        
        let push: PFPush = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
    }
    
    func loadFriendsList() {
        
        let query = PFQuery(className:"Friends")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if error == nil {
                for friendsObj in objects! {
                   // print("friendsObj: ", friendsObj)
                    if (self.friendsList.isEmpty){
                        print("friends list array is empty")
                    }
                    else{
                        self.friendsList = friendsObj["friendsList"] as! [PFUser]
                        print("friend list users count: ", self.friendsList.count)
                        self.tableView.reloadData()
                    }
                    
                }
            }
            else {
                print("Error: ", error)
            }
        }
    }
    
    func loadUserList() {
        let query: PFQuery = PFUser.query()!
     //   query.whereKey("username", notEqualTo: (PFUser.currentUser()?.username!)!)
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in

            if error == nil {
                if let objects = objects as! [PFUser]? {
                    self.userList = objects
                    self.tableView.reloadData()
                }
            } else {
                print("Error: \(error!) \(error!.userInfo)")
                
            }
        }
    }
    
    func loadPendingListUsers() {
        
        let query = PFQuery(className:"Pending")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if error == nil {
                for pendingObj in objects! {
                   // print("pendingObj: ", pendingObj)
                    self.pendingListUsers = pendingObj["friendsPendingList"] as! [PFUser]
                    print("pending list users count: ", self.pendingListUsers.count)
                    self.tableView.reloadData()
                }
                
            }
            else {
                print("Error: ", error)
            }
        }
        
    }
    func loadPicture(user: PFUser, cell: UITableViewCell, rowNumber: Int ) {
        
        cell.imageView!.image = UIImage(named: "FBNoPicture50x50")
        
        if let userPicture = user["profile_picture"] as? PFFile {
            
            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
                if (error == nil) {
                    
                    cell.imageView?.image = UIImage(data:imageData!)
                }
            }
        }
    }

    func filterContentForSearchText(searchText: String, scope: String = "All") {
        
        // Filter loops through and checks for search string
        fileteredUserList = userList.filter { user in
            return user["first_name"].lowercaseString.containsString(searchText.lowercaseString)
        }
        tableView.reloadData()
    }
    
    func createPlusAccessoryView(cell: UITableViewCell ) {
        let imageView       = UIImageView(frame:CGRectMake(18, 18, 18, 18))
        let image           = UIImage(named: "Plus30x30Gold")
        imageView.image     = image
        cell.accessoryView  = imageView
    }
    
    func createMinusAccessoryView(cell: UITableViewCell ) {
        let imageView       = UIImageView(frame:CGRectMake(30, 30, 30, 30))
        let image           = UIImage(named: "Minus30x30.png")
        imageView.image     = image
        cell.accessoryView  = imageView
    }
    
    func createCrossAccessoryView(cell: UITableViewCell ) {
        let imageView       = UIImageView(frame:CGRectMake(20, 20, 20, 20))
        let image           = UIImage(named: "Cross30x30Ruby")
        imageView.image     = image
        cell.accessoryView  = imageView
    }
    
    func setUpCell(cell: UITableViewCell) {
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.textColor = UIColor.yellowColor()
        cell.textLabel?.font = UIFont(name: "ChalkboardSE-Regular", size: 18)!
        cell.detailTextLabel?.textColor = UIColor.yellowColor()
        cell.detailTextLabel?.text = ""
        cell.selectionStyle = .Default
        // cell.selectedBackgroundView = UIView
        // cell.selectedBackgroundView?.backgroundColor = UIColor.blueColor()
        
        let selectionView = UIView()
        selectionView.backgroundColor = UIColor.init(red: 100, green: 100, blue: 107, alpha: 0.6)
        cell.selectedBackgroundView = selectionView
        
        // Rounded images. radius should be half the images width.
        // (cell.imageView?.bounds.size.width)! / 2 is too slow and the first images wont be round
        cell.imageView!.layer.cornerRadius = 22
        cell.imageView!.layer.masksToBounds = true
        cell.imageView!.image = UIImage(named: "FBNoPicture50x50")
    }
    
    func loadRequestsListUsers() {
        
        let query = PFQuery(className:"Requests")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if error == nil {
                for requestsObj in objects! {
                    print("requestsObj: ", requestsObj)
                    self.requestsListUsers = requestsObj["friendsRequestList"] as! [PFUser]
                    print("request list users count: ", self.requestsListUsers)
                    self.tableView.reloadData()
                }
            }
            else {
                print("Error: ", error)
            }
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    if let msg = userInfo["msg"] as? NSObject {
        print(msg)
    }
        print("Push received")
        
}

extension AddFriendTVC: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

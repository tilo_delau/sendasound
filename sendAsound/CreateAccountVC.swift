//
//  CreateAccountVC.swift
//  sendAsound
//
//  Created by IT-Högskolan on 10/04/16.
//  Copyright © 2016 tilo. All rights reserved.
//

import UIKit
import Parse

class CreateAccountVC: UIViewController {

    @IBOutlet var fieldFirstName: UITextField!
    
    @IBOutlet var fieldLastName: UITextField!
    @IBOutlet var fieldPassword: UITextField!
    
    @IBOutlet var btnCreateAccount: UIButton!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(red: 15.0/255.0, green: 47.0/255.0, blue: 107/255.0, alpha: 1.0)
        
        self.hideKeyboardWhenTappedAround()
        
      //  fieldFirstName.borderStyle = .RoundedRect
      //  fieldLastName.borderStyle = .RoundedRect
   /*
        fieldFirstName.backgroundColor = UIColor.whiteColor()
        fieldLastName.backgroundColor = UIColor.whiteColor()
        fieldPassword.backgroundColor = UIColor.whiteColor()
   */     
        
   //     UILabel.appearanceWhenContainedInInstancesOfClasses([UITextField.self]).textColor = UIColor.yellowColor()

        
        fieldFirstName.layer.borderWidth    = 1.0
        fieldLastName.layer.borderWidth     = 1.0
        fieldPassword.layer.borderWidth     = 1.0
     
        fieldFirstName.layer.cornerRadius   = 20
        fieldLastName.layer.cornerRadius    = 20
        fieldPassword.layer.cornerRadius    = 20
        
        fieldFirstName.layer.borderColor    = UIColor.yellowColor().CGColor
        fieldLastName.layer.borderColor     = UIColor.yellowColor().CGColor
        fieldPassword.layer.borderColor     = UIColor.yellowColor().CGColor
/*
        fieldFirstName.textColor            = UIColor.yellowColor()
        fieldLastName.textColor             = UIColor.yellowColor()
        fieldPassword.textColor             = UIColor.yellowColor()
*/        
        btnCreateAccount.layer.cornerRadius = 20
        btnCancel.layer.cornerRadius        = 20
        btnLogin.layer.cornerRadius         = 20
        
        fieldFirstName.attributedPlaceholder = NSAttributedString(string:"First Name",
                                                             attributes:[NSForegroundColorAttributeName: UIColor.yellowColor()])
        fieldLastName.attributedPlaceholder = NSAttributedString(string:"Last Name",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.yellowColor()])
        fieldPassword.attributedPlaceholder = NSAttributedString(string:"Password",
                                                                  attributes:[NSForegroundColorAttributeName: UIColor.yellowColor()])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        
        //textField code
        
        textField.resignFirstResponder()  //if desired
        createAccount()
        return true
    }
    
    @IBAction func actionBtnCreateAccount(sender: AnyObject) {
        
        createAccount()
    }
    @IBAction func actionBtnCancel(sender: AnyObject) {
        
        goBack()
    }
    @IBAction func actionBtnLogin(sender: AnyObject) {
        login()
        
    }
    
    func goBack() {
        navigationController!.popViewControllerAnimated(true)

    }
    
    func createAccount() {
        
        let user            = PFUser()
        let userName        = "\(fieldFirstName.text!) \(fieldLastName.text!)"
        
        user.username       = userName
        
       // user.email          = ""
        user["first_name"]  = fieldFirstName.text
        user["last_name"]  = fieldLastName.text
        
        user.password = fieldPassword.text
                
        if (user.password?.characters.count < 4) {
            
            let alertStr = "Password must be greater than 4 characters"
            
            let alert = UIAlertController(title: "Invalid", message: alertStr, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
        
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            
            var alertStr = String(error)
            
            if let error = error {
                
                let errorCode = error.code
                
                switch errorCode {
                case 100:
                    alertStr = "ConnectionFailed"
                    break
                case 200:
                    alertStr = "Username missing"
                    break
                case 202:
                    alertStr = "Username \(userName) already taken"
                    break
                case 201:
                    alertStr = "Password is missing"
                    break
                default:
                    break
                }
                
                let alert = UIAlertController(title: "Error", message: alertStr, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            
            } else {

                let currentUser = PFUser.currentUser()!
                
                // Two ways to add data
                
               // currentUser.setObject(self.switchEmployer.on, forKey: "employer")
                /*
                 if self.switchEmployer.on   {
                 currentUser.addObject(Bool(true), forKey: "employer") }
                 if !self.switchEmployer.on  { currentUser["employer"] = false }
                 */
                currentUser.saveInBackground()
                let installation = PFInstallation.currentInstallation()
                
                print("logging in")
                installation["user"] = PFUser.currentUser()
                installation.saveInBackground()
                
                let myUser = currentUser
                
                if myUser["requestsList"] == nil {
                    let requests = PFObject(className: "Requests")
                    requests["friendsRequestList"] = []
                    requests["user"] = myUser
                    myUser["requestsList"] = requests
                }
                
                if myUser["pendingList"] == nil {
                    let pending = PFObject(className: "Pending")
                    pending["friendsPendingList"] = []
                    pending["user"] = myUser
                    myUser["pendingList"] = pending
                }
                
                if myUser["friendsList"] == nil {
                    let friend = PFObject(className: "Friends")
                    friend["friendsList"] = []
                    friend["user"] = myUser
                    myUser["friendsList"] = friend
                }
                
                // alert.show()
                
                //self.performSegueWithIdentifier("regToLogin", sender: self)
                self.login()
            }
        }
        }
    }


    func login() {
        
        let userName = "\(fieldFirstName.text!) \(fieldLastName.text!)"
        let passWord = fieldPassword.text
        
       // let user     = PFUser();
        
            //self.fieldPassword.resignFirstResponder()
          //  self.fieldUserName.resignFirstResponder()
        
        print("userName: ", userName)
            PFUser.logInWithUsernameInBackground(userName, password: passWord!, block: { (user, error) -> Void in
                
                if (user) != nil {
                    
                   // self.currentUser = user
                    
                    /*let alert = UIAlertController(title: "Success", message: "Logged in.", preferredStyle: UIAlertControllerStyle.Alert)
                     
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { (action: UIAlertAction) -> Void in */
                    
                    let installation = PFInstallation.currentInstallation()
                    
                    print("logging in")
                    installation["user"] = PFUser.currentUser()
                    installation.saveInBackground()
                    
                   // self.goToTabBarCtrl()
                    /*
                     }))
                     
                     self.presentViewController(alert, animated: true, completion: nil)
                     */
                    
                //    self.navigationController!.popViewControllerAnimated(true)
                    self.goBack()
                    
                } else {
                    print(error)
                    
                    let alert = UIAlertController(title: "Login Failed", message: "Name or Password was incorrect.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
            })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

//
//  Friends.swift
//  sendAsound
//
//  Created by Dexter Talbert on 6/14/16.
//  Copyright © 2016 tilo. All rights reserved.
//

import Foundation
import Parse

class Friends : PFObject, PFSubclassing {
    
    
    override class func initialize() {
        
        struct Static {
            static var onceToken : dispatch_once_t = 0;
            
        }
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    static func parseClassName() -> String {
        return "Friends"
    }
    @NSManaged var friendsList: [PFUser]
    @NSManaged var user:PFUser
}

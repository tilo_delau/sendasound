//
//  FriendsListTVC.swift
//  sendAsound
//
//  Created by IT-Högskolan on 23/01/16.
//  Copyright © 2016 tilo. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class FriendsListTVC: UITableViewController {

    var friendsList = [PFUser]()
//    var friendsData = [[String]]() not using anymore
    var imageCache  = [UIImage]()
    
    var requestsListUsers = [PFUser]()
    var requestsListCount = 0
    
    var selectedUser: PFUser?
    var currentUser: PFUser?
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupIndicator()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func setupIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    override func viewDidAppear(animated: Bool) {
        print("viewDidAppear called")
        self.tableView.reloadData()
        self.activityIndicator.stopAnimating()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        setUp()
        currentUser = PFUser.currentUser()
    }
    
    func setUp() {
        print("do something here")
        
        //loadFriendsList()
        
        
        completionFunction("a parameter") { () -> Void in
           // self.friendsList = ParseHandler.sharedInstance.friendsList
            self.tableView.reloadData()
            print("function completed")
        }
    }

    func completionFunction(param: AnyObject, completion: ()->Void)
    {
        // Do your stuff
        if param is String {
            print("parameter = \(param)")
        }
        loadRequestsListUsers()
        loadFriendsList()
       // ParseHandler.sharedInstance.loadFriendsList()

        print("Friends and Requests loaded. Will reload table")
        
       // cleanUp()
        completion()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
 
        var sectionRowCount = 0

        // Check whether there are friend requests
        if requestsListUsers.count > 0 {
            // There are friend requests
            if section == 0 {
                print("section 0")
                print("section 0 requests count: ", requestsListUsers.count)
                sectionRowCount = requestsListUsers.count
            } else if section == 1 {
                print("section 1 count: ", friendsList.count)
                sectionRowCount = friendsList.count
            }
        } else {
            // There are no friend requests
            if section == 0 {
                print("section 0 friends count: ", friendsList.count)
                sectionRowCount = friendsList.count
            }
        }
        
        return sectionRowCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
        
        setUpCell(cell)
        
        if indexPath.section == 0 && requestsListUsers.count > 0 {
            makeRequestListCells(cell, indexPath: indexPath)

        } else if friendsList.count > 0 {
            makeFriendsListCells(cell, indexPath: indexPath)
            
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Futura", size: 20)!
        header.textLabel?.textColor = UIColor.yellowColor()
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 && requestsListUsers.count > 0 { return "Friend requests:" }
        else { return "Your friends:" }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        if requestsListUsers.count > 0 { return 2 } else { return 1 }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        
        if indexPath.section == 0 && requestsListUsers.count > 0 {
            
            selectedUser = requestsListUsers[indexPath.row]
            
            self.addFriendsComplete("a parameter") { () -> Void in
                self.loadRequestsListUsers()
                self.loadFriendsList()
                
                tableView.reloadData()
                
                self.removeCurrentUserfromPendingList(self.selectedUser!)
                self.addCurrentUserToFriendList(self.selectedUser!)
                
                self.activityIndicator.stopAnimating()
                
                print("function completed")
            }
            
        } else { print("(no requests) This is section: ", indexPath.section) }
        
        print("cell selected: ", cell?.textLabel?.text)
    }
    
    func addFriendsComplete(param: AnyObject, completion: ()->Void) {
        
        self.activityIndicator.startAnimating()
        
        self.addUserToFriendList(self.selectedUser!)
        self.removeUserFromRequestList(self.selectedUser!)
        
        if param is String {
            print("parameter = \(param)")
        }
        completion()
    }
    
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        print("cell Deselected: ", cell?.textLabel?.text)
    }
    
    func makeFriendsListCells(cell: UITableViewCell, indexPath: NSIndexPath) {
        
        let friend = friendsList[indexPath.row]
        
        friend.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            cell.textLabel?.text = "\(friend["first_name"]) \(friend["last_name"])"
            cell.accessoryView?.hidden = true

            dispatch_async(dispatch_get_main_queue()) {
                self.loadPicture(friend, cell: cell, rowNumber: indexPath.row)
                
                if friend["first_name"].isEqualToString("Tilo-Karl") {
                    cell.detailTextLabel?.text = "Creator"
                }
            }
        })
    }
    
    func makeRequestListCells(cell: UITableViewCell, indexPath: NSIndexPath) {
        
        let user = requestsListUsers[indexPath.row]
        createPlusAccessoryView(cell)
        
        user.fetchIfNeededInBackgroundWithBlock({ (object,error) -> Void in
            cell.textLabel?.text = "\(user["first_name"]) \(user["last_name"])"
                dispatch_async(dispatch_get_main_queue()) {
                    self.loadPicture(user, cell: cell, rowNumber: indexPath.row)
                }
        })
    }
    
    func createPlusAccessoryView(cell: UITableViewCell ) {
        let imageView       = UIImageView(frame:CGRectMake(18, 18, 18, 18))
        let image           = UIImage(named: "Plus30x30Gold.png")
        imageView.image     = image
        cell.accessoryView  = imageView
    }
    
    func createMinusAccessoryView(cell: UITableViewCell ) {
        let imageView       = UIImageView(frame:CGRectMake(18, 18, 18, 18))
        let image           = UIImage(named: "Minus30x30.png")
        imageView.image     = image
        cell.accessoryView  = imageView
    }
 /*
    func loadRequestListCount() {
        let currentUserRequestsList = PFUser.currentUser()?.objectForKey("requestsList")
        
        // why fetchifneeded? why not find objects in background?
        currentUserRequestsList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in

            if let count = currentUserRequestsList!["friendsRequestList"]!!.count {
                self.requestsListCount = count
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        })
    }
*/
    func loadPicture(user: PFUser, cell: UITableViewCell, rowNumber: Int ) {
        
        cell.imageView!.image = UIImage(named: "FBNoPicture50x50")
        
        if let userPicture = user["profile_picture"] as? PFFile {
            
            userPicture.getDataInBackgroundWithBlock { (imageData: NSData?, error: NSError?) -> Void in
            if (error == nil) {
                
                    cell.imageView?.image = UIImage(data:imageData!)
                }
            }
        }
    }
 
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
   
        if indexPath.section == 0 && requestsListUsers.count > 0 {
            print("Request list user cells are not editable")
            return false
        }
        return true
    }

  
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        print("deleting")

        if editingStyle == UITableViewCellEditingStyle.Delete {

            if indexPath.section == 0 && requestsListUsers.count > 0 {
                print("Trying to delete a request. Do nothing")

            } else {
                print("friends count: ", friendsList.count)
                selectedUser = friendsList[indexPath.row]
                
                print("Selected user to be deleted: ", selectedUser?.username)
                print("index row to be deleted: ", indexPath.row)
                //removeCurrentUserfromPendingList(selectedUser!)
                //removeUserFromRequestList(selectedUser!)
                
                deleteFriend(selectedUser!)
                
                removeUserFromFriendsList(selectedUser!)
                removeCurrentUserFromFriendsList(selectedUser!)
                friendsList.removeAtIndex(indexPath.row)
              //  requestsListUsers.removeAtIndex(indexPath.row)
                print("friends count2: ", friendsList.count)
                self.loadRequestsListUsers()
                self.loadFriendsList()
                
            }
            

           // self.tableView.reloadData()
            
         /*
            objectToDelete.deleteInBackgroundWithBlock {
                (success: Bool, error: NSError?) -> Void in
                if (success) {
                    // Force a reload of the table - fetching fresh data from Parse platform
                    self.loadFriendsList()
                } else {
                    // There was a problem, check error.description
                }
            }
*/
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func deleteFriend(selectedUser: PFUser) {
        deleteFriendWithCompletion(selectedUser) { (result: Bool) -> Void in
            if result ==  true {
                
            }
            if result == false {
                
            }
        }
    }
    
    func deleteFriendWithCompletion(selectedUser: PFUser, completion: (result: Bool) -> Void){
        
        let selectedUserPendingList = selectedUser.objectForKey("pendingList")
        
        selectedUserPendingList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            
            if error == nil {
                completion(result: true)
            } else {
                completion(result: false)
            }
            
            })
        
    }

    
    /*
    *  add me to his friends list
    add him to my friends list
    remove me from his pending list
    remove him from my request list
    
    *
    */
    func removeCurrentUserfromPendingList(selectedUser: PFUser) {
        
        print("removeCurrentUserfromPendingList: ", selectedUser.email)
        
        let selectedUserPendingList = selectedUser.objectForKey("pendingList")
        selectedUserPendingList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            print("SelectedUserPendingList: ", selectedUserPendingList?.objectId)
            selectedUserPendingList?.removeObject(self.getCurrentUser(), forKey: "friendsPendingList")
            selectedUserPendingList?.saveInBackground()
        })
    }
    
    func removeUserFromRequestList(selectedUser: PFUser) {
        
        print("removeUserFromRequestList: ", selectedUser)
        let currentUserRequestList = getCurrentUser().objectForKey("requestsList")
        currentUserRequestList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            print("currentUserRequestList: ", currentUserRequestList!.objectId)
            currentUserRequestList?.removeObject(selectedUser, forKey: "friendsRequestList")
            currentUserRequestList?.saveInBackground()
        })
    }
    func removeUserFromFriendsList(selectedUser: PFUser) {
        
        print("removeUserFromFriendsList")
        let currentUserRequestList = getCurrentUser().objectForKey("friendsList")
        currentUserRequestList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            currentUserRequestList?.removeObject(selectedUser, forKey: "friendsList")
            currentUserRequestList?.saveInBackground()
            
        })
    }
    
    func removeCurrentUserFromFriendsList(selectedUser: PFUser) {
        
        print("removeCurrentUserFromFriendsList")
        let selectedUserfriendsList = selectedUser.objectForKey("friendsList")
        selectedUserfriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            selectedUserfriendsList!.removeObject(PFUser.currentUser()!, forKey: "friendsList")
            selectedUserfriendsList!.saveInBackground()
        })        
    }
    
    func addCurrentUserToFriendList(selectedUser: PFUser) {
        
        print("addCurrentUserToFriendList")
        let selectedUserfriendsList = selectedUser.objectForKey("friendsList")
        selectedUserfriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            selectedUserfriendsList!.addUniqueObject(self.getCurrentUser(), forKey: "friendsList")
            selectedUserfriendsList!.saveInBackground()
        })
    }
    func addUserToFriendList(selectedUser: PFUser) {

        print("addUserToFriendList")
        let currentUserFriendsList  = getCurrentUser().objectForKey("friendsList")
        currentUserFriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            currentUserFriendsList?.addUniqueObject(selectedUser, forKey: "friendsList")
            currentUserFriendsList?.saveInBackground()
            
        })
  //      pushNotifiction(selectedUser)
    }
    
    func getCurrentUser() -> PFUser {
        
        
/*
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        print("addUserToFriendList")
        do {
            self.currentUser = try PFUser.currentUser()!.fetch()
            print("currentuser: ", self.currentUser)
            
        } catch { print("Fetch failed")
        }
        }
*/
    /*
        let currentUser  = getCurrentUser().objectForKey("friendsList")
        currentUserFriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            currentUserFriendsList?.addUniqueObject(selectedUser, forKey: "friendsList")
            currentUserFriendsList?.saveInBackground()
            
        })
        
   */
        return self.currentUser!
            
        
    }
    /*
    func addUserToFriendList(selectedUser: PFUser) {
        
        print("addUserToFriendList")
        let currentUserFriendsList  = PFUser.currentUser()?.objectForKey("friendsList")
        currentUserFriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            currentUserFriendsList?.addUniqueObject(selectedUser, forKey: "friendsList")
            currentUserFriendsList?.saveInBackground()
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print("User added to friend list, dispatched main queue, updating...")
              //  self.loadRequestsListUsers()
               // self.loadFriendsList()
            })
        })
        pushNotifiction(selectedUser)
    }
  */
    
    
    func loadFriendsList() {
        let currentUserFriendsList  = PFUser.currentUser()?.objectForKey("friendsList")
        currentUserFriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            
           // print("currentUserFriendsList!['friendsList']: ",currentUserFriendsList?.whereobj
            
            self.friendsList = currentUserFriendsList?.objectForKey("friendsList") as! [PFUser]
            print("friends: ", self.friendsList)
            
        })
    }


/*
    func loadRequestsListUsers() {
        
        let currentUserRequestsList  = PFUser.currentUser()?.objectForKey("requestsList")
        currentUserRequestsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            
            print("currentUserRequestsList!['requestsList']: ",currentUserRequestsList!["friendsRequestList"])
            
            self.requestsListUsers = currentUserRequestsList!["friendsRequestList"] as! [PFUser]
            
            print("requests: ", self.requestsListUsers)
            
            self.tableView.reloadData()
            
        })
    }
*/    


    
/*
        let query = PFQuery(className:"Friends")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if error == nil {
                for friendsObj in objects! {
                    
                    self.friendsList.append(friendsObj as! PFUser)
                
                    //print("friendsObj: ", friendsObj)
            //        self.friendsList = friendsObj["friendsList"] as! [PFUser] // this should be object
                    print("loadFriendsList, count: ", self.friendsList.count)
                    
                   // self.loadFriendsData(friendsObj["friendsList"] as! [PFUser])
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        //self.cleanUp()
                        self.tableView.reloadData()
                    })
                }
            }
            else {
                print("Error: ", error)
            }
        }

    }
*/

    
    func loadRequestsListUsers() {
        
        let query = PFQuery(className:"Requests")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if error == nil {
                for requestsObj in objects! {
                   // print("requestsObj: ", requestsObj)
                    self.requestsListUsers = requestsObj["friendsRequestList"] as! [PFUser]
                    print("loadRequestsListUsers, count: ", self.requestsListUsers.count)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                       // self.cleanUp()
                        self.tableView.reloadData()
                    })
                }
            }
            else {
                print("Error: ", error)
            }
        }
    }
    
    func pushNotifiction(receiver: PFUser) {
        
        let message = " \(PFUser.currentUser()!["first_name"]!) added you as a friend!"
        
        let data = [
            "alert": message,
            "sound": "default"]
        
        let query: PFQuery = PFInstallation.query()!
        query.whereKey("user", equalTo: receiver)
        
        let push: PFPush = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]){
   
    }
    
    // MARK: - Table View Cell creation
    func setUpCell(cell: UITableViewCell) {
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.textColor = UIColor.yellowColor()
        cell.textLabel?.font = UIFont(name: "ChalkboardSE-Regular", size: 18)!
        cell.detailTextLabel?.textColor = UIColor.yellowColor()
        cell.detailTextLabel?.text = ""
        cell.selectionStyle = .Default
        // cell.selectedBackgroundView = UIView
        // cell.selectedBackgroundView?.backgroundColor = UIColor.blueColor()
        
        let selectionView = UIView()
        selectionView.backgroundColor = UIColor.init(red: 100, green: 100, blue: 107, alpha: 0.6)
        cell.selectedBackgroundView = selectionView
        
        // Rounded images. radius should be half the images width.
        // (cell.imageView?.bounds.size.width)! / 2 is too slow and the first images wont be round
        cell.imageView!.layer.cornerRadius = 22
        cell.imageView!.layer.masksToBounds = true
        cell.imageView!.image = UIImage(named: "FBNoPicture50x50")
    }
   
    func cleanUp() {
        var index = 0
        
        print("CLEAN UP CALLED")
        
        for friend in requestsListUsers {
            if friendsList.contains(friend) {
                requestsListUsers.removeAtIndex(index)
                removeUserFromRequestList(friend)
                index += 1
                print("Pend List users count: ", requestsListUsers.count)
            }
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  Logout.swift
//  sendAsound
//
//  Created by IT-Högskolan on 27/01/16.
//  Copyright © 2016 tilo. All rights reserved.
//

import UIKit
import Parse

class Logout: UIViewController {
    
    var currentUser: PFUser?

    override func viewDidAppear(animated: Bool) {
        PFUser.logOut()
        
        if let tabBarController = self.navigationController?.viewControllers[0] as? TabBarController {
            tabBarController.disableTabBarButtons(true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //PFUser.logOut()
        //PFUser.currentUser() = nil
        
        /*
        
        if currentUser != nil {
            
            PFUser.logOutInBackgroundWithBlock({ (error) -> Void in
                if (error != nil) {
                    print("Couldn't log out")
                } else {
                    print("Logged out")
                    
                    self.currentUser = PFUser.currentUser()
                    
                    if self.currentUser == nil {
                        print("currentuser: ", self.currentUser)
                        
                        /*let alert = UIAlertController(title: "Success", message: "Logged out.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)*/
                    }
                }
                
            })
            
        }
*/
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

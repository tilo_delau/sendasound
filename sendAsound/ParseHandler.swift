//
//  ParseHandler.swift
//  sendAsound
//
//  Created by IT-Högskolan on 24/02/16.
//  Copyright © 2016 tilo. All rights reserved.
//

import Foundation
import Parse
import ParseFacebookUtilsV4

class ParseHandler {

    var friendsList = [PFUser]()
    
    class var sharedInstance: ParseHandler {
        
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: ParseHandler? = nil
        }
        
        dispatch_once(&Static.onceToken) {
            Static.instance = ParseHandler()
            Static.instance?.loadFriendsList()
        }
        
        return Static.instance!
    }

    func loadRequestListCount() {
/*
        let currentUserRequestsList = PFUser.currentUser()?.objectForKey("friendList")

        currentUserRequestsList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            currentUserRequestsList?.addUniqueObject(selectedUser, forKey: "friendList")
            currentUserRequestsList!.saveInBackground()
            print("Pending List Count: ", currentUserRequestsList!["friendsPendingList"]!!.count)
            
            self.loadPendingListUsers()
            
        })
*/
    }
    
    func loadFriendsList() {
/*
        let user = PFUser.currentUser()

        if user == nil {
            return
        }
                
        let currentUserFriendsList  = PFUser.currentUser()?.objectForKey("friendsList")
        currentUserFriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            self.friendsList = currentUserFriendsList!["friendsList"] as! [PFUser]
            
        })
*/
        if PFUser.currentUser() != nil {
            let query = PFQuery(className:"Friends")
            
            query.whereKey("user", equalTo: PFUser.currentUser()!)
            
            query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
                
                if error == nil {
 
                    for friendsObj in objects! {
                        self.friendsList = friendsObj["friendsList"] as! [PFUser]
                    }
                }
                else {
                    print("Error: ", error)
                }
            }
        }
    }

}
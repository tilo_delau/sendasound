//
//  StartVC.swift
//  sendAsound
//
//  Created by IT-Högskolan on 17/12/15.
//  Copyright © 2015 tilo. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4
import FBSDKLoginKit
import FBSDKCoreKit
import AVFoundation



class StartVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,FBSDKLoginButtonDelegate {
    
    @IBOutlet var viewFB: UIView!
    
    @IBOutlet var btnFBLogin: UIButton!
    var sampleButton:UIButton?
    
    @IBOutlet var viewBtnFB: UIView!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var pickerViewSound: UIPickerView!
    
    @IBOutlet var btnSendASound: UIButton!
    //var pickerData: [String] = [String]()
    var pickerData: [[String]] = [[String]]()
    //let btnFB = FBSDKLoginButton.init()
    
    var selectedSound: String?
    var selectedFriend: String?
    var selectedFriendUser: PFUser?
    
    var friendsList = [PFUser]()
    
    var noFriendStr = "Find a friend!"
    
    var myTimer : NSTimer?
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    
    func toggleBtn() {
        
        print("togglebtn")
        print("state: ", btnSendASound.state)
        //   btnSendASound.enabled = true
        //   btnSendASound.reloadInputViews()
        print("state: ", btnSendASound.state)
        
        if btnSendASound.enabled == false {
            btnSendASound.enabled = true
        } else {
            btnSendASound.enabled = false
        }
        
    }
    
    func setupIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        btnSendASound.enabled = true
        
        makeLoginButton()
        
        let currentInstallation = PFInstallation.currentInstallation()
        
        if currentInstallation.badge != 0 {
            currentInstallation.badge = 0
            currentInstallation.saveEventually()
        }
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        
        
        
        //   var timer = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: "update", userInfo: nil, repeats: true)
        
        
        /*
         print("FriendsList: ", friendsList)
         print("user: ", PFUser.currentUser()?.email)
         print("My friends: ", PFUser.currentUser()?.objectForKey("friendsList"))
         */
        
        
        //  updatePicker()
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH  < 568.0 {
            
            print("IPHONE 4")
            
            pickerViewSound.frame=CGRectMake(0, 50, view.frame.width, 50)
            pickerView.frame=CGRectMake(0, 50, view.frame.width, 50)
            
            pickerViewSound.contentMode = .Redraw
            
            pickerViewSound.frame.size.height = 50
            // pickerViewSound.frame.size.width = 50
            
        }
        
        
        
    }
    
    // Kills status bar for this controller only.
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        //setupIndicator()
        print("viewWillAppear called")
        if PFUser.currentUser() != nil {
            
            loadFriendsList()
            
            /*
             var friendsList = [PFUser]()
             
             ParseHandler.sharedInstance.loadFriendsList()
             friendsList = ParseHandler.sharedInstance.friendsList
             print("friendsList2 count:", friendsList.count)
             self.setPickerFriends()
             */
            //
            //            if friendsList.isEmpty {
            //                initPicker([noFriendStr])
            //            }
            
            
        }
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            let sharedTabBarController = self.navigationController as! NavCtrl
            sharedTabBarController.shouldRotate = false
            
        }
        
        //pickerViewSound.frame.size.height
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        
        let sharedTabBarController = self.navigationController as! NavCtrl
        sharedTabBarController.shouldRotate = true
        
        //sampleButton!.frame.size.height = 96
        selectedFriendUser = nil // Have to clear this or the old friend will still be here next time
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        toggleFBButton()


        
        
        /*
         
         if  FBSDKAccessToken.currentAccessToken()?.userID != nil {
         print("FB Token not nil: ", FBSDKAccessToken.currentAccessToken().userID)
         viewFB.sendSubviewToBack(viewFB)
         viewFB.hidden = true
         btnFBLogin.hidden = true
         
         print("currentUser: ", PFUser.currentUser())
         
         } else if PFUser.currentUser() == nil  {
         print("FB Token is nil")
         viewFB.bringSubviewToFront(viewFB)
         }
         */
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // btnFB.center = view.center
        viewBtnFB.center = view.center
        toggleFBButton()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
            print("landscape")
            
        } else {
            print("portrait")
            
            /*
             let horizontalConstraint = btnSendASound.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor)
             let verticalConstraint = btnSendASound.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor)
             let widthConstraint = btnSendASound.widthAnchor.constraintEqualToAnchor(nil, constant: 100)
             let heightConstraint = btnSendASound.heightAnchor.constraintEqualToAnchor(nil, constant: 100)
             
             NSLayoutConstraint.activateConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
             */
            // NSLayoutConstraint.activateConstraints(<#T##constraints: [NSLayoutConstraint]##[NSLayoutConstraint]#>)
        }
    }
    
    func getSound() -> String {
        
        var soundFileName: String?
        
        switch selectedSound! {
        case "Fart":                soundFileName = "fart-2.wav"
        case "I'm hungry":          soundFileName = "im-so-hungry.wav"
        case "Open the door!":      soundFileName = "open-the-door.wav"
        case "What are you doing?": soundFileName = "what-are-you-doing.wav"
        case "Where are we going?": soundFileName = "where-are-we-going.wav"
        case "Almost time":         soundFileName = "its-almost-time.wav"
        case "No. No, no!":         soundFileName = "no-3.wav"
        case "Yeah":                soundFileName = "yeah.wav"
        case "Go shopping":         soundFileName = "wanna-go-shopping.wav"
        case "TGIF!":               soundFileName = "thank-god-its-friday.wav"
        case "Kill you":            soundFileName = "I-will-kill-you.wav"
        case "OMG!":                soundFileName = "female_saying_oh_my_god.wav"
        case "Wolf whistle":        soundFileName = "wolf-whistle.wav"
        case "Happy New Year!":     soundFileName = "happy-new-year.wav"
        case "I just got here":     soundFileName = "i-just-got-here.wav"
        default:                    soundFileName = "soundPersonFarting.wav"
        }
        return soundFileName!
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("FBLogin button pressed")
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("FBLogout button pressed")
    }
    
    // MARK: Picker View Sound
    func pickerViewSound(pickerViewSound: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }
    func pickerViewSound(pickerViewSound: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("pickerViewSound title")
        return pickerData[component][row]
    }
    
    
    // MARK: Picker View
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        if pickerView.tag == 1 { return 1 }
        if pickerView.tag == 2 { return 1 }
        
        
        // return pickerData.count
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 2 { return pickerData[1].count }
        return pickerData[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        print("picker data: ",pickerData[1][0])
        if pickerView.tag == 2 { return pickerData[1][row] }
        
        return pickerData[component][row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        /*
         if friendsList.isEmpty {
         noFriendStr = "Find a friend first!"
         //initPicker([noFriendStr])
         return
         }
         */
        if pickerView.tag == 1 {
            selectedSound = pickerData[0][row]
            print("Selected Sound: ", selectedSound!)
        }
        
        // print("picker selected: ", pickerData[1][row])
        if pickerView.tag == 2 {
            
            
            if friendsList.isEmpty {
                return
            }
            if pickerData[1][row] != noFriendStr || !pickerData[1][row].isEmpty {
                selectedFriend = pickerData[1][row]
                selectedFriendUser = friendsList[row]
                print("Selected Friend: ", selectedFriend!)
            }
        }
        //print("Selected Friend: ", pickerData[1][row])
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        
        var attributedString = NSAttributedString(string: pickerData[component][row], attributes: [NSForegroundColorAttributeName : UIColor.yellowColor()])
        
        if pickerView.tag == 2 {
            attributedString = NSAttributedString(string: pickerData[1][row], attributes: [NSForegroundColorAttributeName : UIColor.yellowColor()])
        }
        return attributedString
    }
    
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel;
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            
            // pickerLabel?.font = UIFont(name: "ChalkboardSE-Bold", size: 25)
            pickerLabel?.font = UIFont(name: "Futura", size: 22)
            pickerLabel?.textAlignment = NSTextAlignment.Center
            pickerLabel?.textColor  = UIColor.yellowColor()
        }
        
        pickerLabel?.text = pickerData[component][row]
        if pickerView.tag == 2 {
            pickerLabel?.text = pickerData[1][row]
        }
        
        return pickerLabel!;
    }
    
    // MARK: Actions
    @IBAction func actionSendASound(sender: AnyObject) {
        
        self.pickerView.reloadAllComponents()
        self.pickerViewSound.reloadAllComponents()
        
        
        let application: UIApplication = UIApplication.sharedApplication()
        
 
        
        let types: UIUserNotificationType = [.Alert, .Badge, .Sound]
        let settings = UIUserNotificationSettings(forTypes: types, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        
        // Need to either loadfriends or check selectedFriendUser or something.
        // Its not updating
        
        if selectedFriendUser == nil {
            print("no friend!")
            
            if !friendsList.isEmpty {
                selectedFriendUser = friendsList[0]
                pushNotifiction(selectedFriendUser!)
            }
        }
        else {
            
            pushNotifiction(selectedFriendUser!)
            print("friendUser: ", selectedFriendUser?.objectId)
        }
        
    }
    
    func pushNotifiction(receiver: PFUser) {
        
        btnSendASound.enabled = false
        myTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(StartVC.toggleBtn), userInfo: nil, repeats: false)
        
        let message = " \(PFUser.currentUser()!["first_name"]!) sent you '\(selectedSound!)'"
        let sound   = getSound()
        let data = [
            "alert": message,
            "sound": sound,
            "badge": "increment"]
        
        let query: PFQuery = PFInstallation.query()!
        query.whereKey("user", equalTo: receiver)
        
        let push: PFPush = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
    }
    
    @IBAction func actionFBLogin(sender: AnyObject) {
        
        print("actionFBLogin called")
        
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile", "email"],
                                                             block: {
                                                                (user: PFUser?, error: NSError?) -> Void in
                                                                
                                                                if error != nil {
                                                                    let myAlert = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
                                                                    
                                                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                                                    myAlert.addAction(okAction)
                                                                    self.presentViewController(myAlert, animated: true, completion: nil);
                                                                    return
                                                                }
                                                                
                                                                if let user = user {
                                                                    if user.isNew {
                                                                        print("User signed up and logged in through Facebook!")
                                                                        print("user new id: \(user.objectId)")
                                                                        self.saveUserToParse()
                                                                        
                                                                        
                                                                    } else {
                                                                        print("User logged in through Facebook!")
                                                                        print("user id: \(user.objectId)")
                                                                        self.saveUserToParse()
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    //self.updatePicker()
                                                                    
                                                                    
                                                                    
                                                                } else {
                                                                    print("Uh oh. The user cancelled the Facebook login.")
                                                                }
                                                                self.toggleFBButton()
        })
    }
    
    func toggleFBButton() {
        
        if PFUser.currentUser() == nil || PFUser.currentUser()?.username == nil {
            
            viewFB.bringSubviewToFront(viewFB)
            viewFB.hidden = false
            viewFB.alpha = 0.4
            
            
            viewBtnFB.bringSubviewToFront(viewBtnFB)
            viewBtnFB.alpha = 1
            viewBtnFB.hidden = false
            
            btnFBLogin.bringSubviewToFront(btnFBLogin)
            // btnFBLogin.alpha = 1
            btnFBLogin.hidden = false
            
            sampleButton?.hidden = false
            
        }
        
        if PFUser.currentUser() != nil {
            viewFB.sendSubviewToBack(viewFB)
            viewFB.hidden = true
            viewFB.alpha = 0
            
            btnFBLogin.sendSubviewToBack(btnFBLogin)
            btnFBLogin.hidden = true
            
            viewBtnFB.sendSubviewToBack(viewBtnFB)
            viewBtnFB.hidden = true
            
            sampleButton?.hidden = true
            
            if let tabBarController = self.navigationController?.viewControllers[0] as? TabBarController {
                tabBarController.disableTabBarButtons(false)
            }
            
        }
        
    }
    
    func saveUserToParse() {
        let requestParameters = ["fields": "id, email, first_name, last_name"]
        
        let userDetails = FBSDKGraphRequest(graphPath: "me", parameters: requestParameters)
        
        userDetails.startWithCompletionHandler { (connection, result, error:NSError!) -> Void in
            
            if error != nil {
                print("\(error.localizedDescription)")
                return
            }
            
            if result != nil {
                let userId: String?         = result.valueForKey("id") as? String
                let userFirstName:String?   = result.valueForKey("first_name") as? String
                let userLastName:String?    = result.valueForKey("last_name") as? String
                let userEmail:String?       = result.valueForKey("email") as? String
                print("user mail: \(userEmail)")
                
                let myUser: PFUser = PFUser.currentUser()!
                
                // Save user properties
                if userFirstName != nil {
                    myUser.setObject(userFirstName!, forKey: "first_name")
                }
                
                if userLastName != nil {
                    myUser.setObject(userLastName!, forKey: "last_name")
                }
                
                if userEmail != nil {
                    myUser.setObject(userEmail!, forKey: "email")
                }
                
                if myUser["requestsList"] == nil {
                    let requests = PFObject(className: "Requests")
                    requests["friendsRequestList"] = []
                    requests["user"] = myUser
                    myUser["requestsList"] = requests
                }
                
                if myUser["pendingList"] == nil {
                    let pending = PFObject(className: "Pending")
                    pending["friendsPendingList"] = []
                    pending["user"] = myUser
                    myUser["pendingList"] = pending
                }
                
                if myUser["friendsList"] == nil {
                    let friend = PFObject(className: "Friends")
                    friend["friendsList"] = []
                    friend["user"] = myUser
                    myUser["friendsList"] = friend
                }
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    
                    // Get Facebook profile picture
                    let userProfile = "https://graph.facebook.com/" + userId! + "/picture?type=square"
                    
                    let profilePictureUrl = NSURL(string: userProfile)
                    
                    let profilePictureData = NSData(contentsOfURL: profilePictureUrl!)
                    
                    if(profilePictureData != nil) {
                        let profileFileObject = PFFile(data:profilePictureData!)
                        myUser.setObject(profileFileObject!, forKey: "profile_picture")
                    }
                    
                    myUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                        
                        if(success) {
                            print("User details are now updated")
                            self.loadFriendsList()
                            
                            // May need to point the installation to the user for push later
                            let installation = PFInstallation.currentInstallation()
                            
                            installation["user"] = PFUser.currentUser()
                            installation.saveInBackground()
                            
                        }
                        
                    })
                    
                }
                
            }
            
        }
    }
    
    func loadFriendsList() {
        if PFUser.currentUser() != nil {
            //var userFriendsObject = Friends()
            //var userFriends = Friends()
            // userFriends.friendsList = nil
            
            print("loading friends... ")
            let query = PFQuery(className:"Friends")
            query.whereKey("user", equalTo: PFUser.currentUser()!)
            query.includeKey("friendsList");
            query.getFirstObjectInBackgroundWithBlock{(object: AnyObject?, error: NSError?) -> Void in
                
                if (error == nil) {
                    if (object == nil){
                        print("Object is nil, User is prbably new and has no friends yet")
                    }
                    
                    if object != nil {
                        
                        print("object is not nil: ", object) // I can see friendsList here but friendslist still considered nil.
                        print("object is not nil, flist is: ", object?.friendsList)
                        print("object value of key: ", object?.valueForKey("friendsList"))
                        
                        var objFriendsList = [PFUser]()
                        objFriendsList = (object?.valueForKey("friendsList"))! as! [PFUser]
                        // Dont print below because it will just stop if objFriendsList is empty
                        //print("objName: ", objFriendsList[0].objectForKey("first_name"))
                        self.friendsList = objFriendsList
                        
                        
                        if object?.friendsList == nil {
                            print("object?.friendsList is nil")
                            print("object.friendsList: ", object?.friendsList)

                        }
                        
                    }

                    if ((object?.friendsList != nil)){
                        //self.friendsList = (object?.friendsList)!
                        print("There are some friends so copy them: ")
                        print("object.friendsList: ", object?.friendsList)
                        print("Object: ", object)
                        
                    }
                    
                    self.setPickerFriends()
                }
                else {
                    print("Error: ", error)
                }
                
            }
            
            //get 1st name
            
            /*
             for user in userFriends.friendsList{
             var theUser = user as! PFUser
             print ("\(theUser.objectForKey("first_name") as! String)")
             }
             */
            
            //sort the name
            //userFriends.friendsList.sort { $0.first_name < $1.first_name }
            
            
        }
        /*
         if PFUser.currentUser() != nil {
         let query = PFQuery(className:"Friends")
         query.whereKey("user", equalTo: PFUser.currentUser()!)
         query.orderByAscending("first_name");
         query.includeKey("friendsList");
         
         query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
         
         
         if error == nil {
         for friendsObj in objects! {
         print("friendsObj: ", friendsObj.objectForKey("user"))
         self.friendsList = friendsObj["friendsList"] as! [PFUser]
         
         print("friendslist.count2: ", self.friendsList.count)
         //self.setPickerFriends()
         }
         
         }
         else {
         print("Error: ", error)
         }
         print("friendslist.count: ", self.friendsList.count)
         self.setPickerFriends()  // doesnt work unless called in here and then it crashes
         self.activityIndicator.stopAnimating()
         
         /*
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
         //self.setPickerFriends()  // doesnt work unless called in here and then it crashes
         
         }
         */
         
         }
         
         }
         }
         */
    }
    /*
     func loadFriendsList() {
     
     let currentUserFriendsList  = PFUser.currentUser()?.objectForKey("friendsList")
     //currentUserFriendsList!.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
     //currentUserFriendsList!.findObjectsInBackgroundWithBlock({ (object, error) -> Void in
     // print("currentUserFriendsList!['friendsList']: ",currentUserFriendsList?.whereobj
     
     self.friendsList = currentUserFriendsList?.objectForKey("friendsList") as! [PFUser]
     print("loadFriendsList ", self.friendsList)
     
     self.setPickerFriends()
     
     })
     }
     */
    /*
     func loadFriendsList() {
     let currentUserFriendList = PFUser.currentUser()!.objectForKey("friendsList")
     
     currentUserFriendList?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
     
     
     if error == nil {
     print("friendsList  Count: ", currentUserFriendList!["friendsList"]!!.count)
     print("friendsList: ", currentUserFriendList!["friendsList"]!)
     
     for friend in currentUserFriendList!["friendsList"]! {
     
     }
     
     }
     else {
     print("Error: ", error)
     }
     //  currentUserFriendList?.addUniqueObject(selectedUser, forKey: "friendsList")
     // currentUserPendingList!.saveInBackground()
     
     
     
     //   self.loadPendingListUsers()
     //   self.tableView.reloadData()
     
     })
     
     //pushNotifiction(selectedUser)
     }
     */
    
    
    func setPickerFriends() {
        print("setPickerFriends called")
        //print("CurrentUSer is: ", PFUser.currentUser())
        var userList = [String]()

        print("setPicker friendsList: ", friendsList)
        if self.friendsList.isEmpty {
            userList.append((noFriendStr))
            self.initPicker(userList)
            return
        }
        else {
            
            userList.removeAll() // remove notFriendStr
            
            let friendsCount = friendsList.count
            var userList2 = [String]()
            for index in 0..<friendsCount {
                let firstName = self.friendsList[index].objectForKey("first_name") as! String!
                userList2.append(firstName)
                
            }
            print("userList2: ", userList2)
            self.initPicker(userList2)
            
            
            
            //var range = self.friendsList.count
            /*
             for index in 0..<2 {
             var userList2 = [String]()
             userList2[index] = self.friendsList[index].objectForKey("first_name")
             print(userList2)
             index += 1
             }
             */
            
            /*
             var friendCount = 0
             for friend in friendsList {
             
             print("userlist1: ", userList)
             
             // userList.append(friend)
             friend.fetchInBackgroundWithBlock({ (object, error) in
             
             //  var friendName = friend.objectForKey("first_name")
             print("friendListindex0, ", self.friendsList[0].objectForKey("first_name"))
             print("FRIEND \(friendCount): ", friend.objectForKey("first_name"))
             
             userList.insert(friend.objectForKey("first_name") as! String, atIndex: friendCount)
             
             
             //userList.append(friend.objectForKey("first_name") as! String)
             //print("friendListTTTTTTTTT: ", self.friendsList)
             
             print("userListindex0, ", userList[0])
             print("userlist2: ", userList)
             
             
             self.initPicker(userList)
             friendCount += 1
             
             })
             
             
             //friend.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
             // print("friend: ", friend)
             
             //})
             }
             */
        }
        
    }
    
    
    func updatePicker() { // not using atm
        //get parse users
        let query = PFUser.query()
        var userList = [String]()
        
        // view.addSubview(btnFB)
        // button1.addTarget(self, action: "buttonClicked:", forControlEvents: .TouchUpInside)
        //btnFB.addTarget(self, action: "actionFBLogin", forControlEvents: .TouchUpInside)
        
        query?.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            if error == nil {
                if let objects = objects {
                    for obj in objects {
                        print("adding friends by id to picker view: ", obj.objectId)
                        userList.append(obj.objectForKey("first_name") as! String)
                        
                        self.initPicker(userList)
                    }
                }
            } else {
                print("An error occurred")
            }
        })
    }
    
    func initPicker(userList: [String]) {
        
        // Create the data array:
        pickerData = [["Fart", "I'm hungry", "Open the door!", "What are you doing?", "Almost time", "Go shopping", "TGIF!", "OMG!", "Where are we going?", "I just got here", "No. No, no!", "Yeah", "Kill you", "Wolf whistle", "Happy New Year!"],
                      userList]
        
        // Connect data:
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        self.pickerViewSound.delegate = self
        self.pickerViewSound.dataSource = self
        
        print("userList init: ", pickerData[1])
        
        // Sets selectedSound so no nil, maybe don't need to animated it
        self.pickerViewSound.selectRow(0, inComponent: 0, animated: true)
        // Sets selected friend on the picker to 0
        self.pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerView(self.pickerViewSound, didSelectRow: 0, inComponent: 0)
        pickerView.reloadAllComponents()
        // The selection indicator to yellow
        
        // pickerView.subviews[1].backgroundColor = UIColor.yellowColor()
        //pickerView.subviews[2].backgroundColor = UIColor.yellowColor()
        
        
    }
    
    func makeLoginButton() {
        
        
        //sampleButton = UIButton(frame: CGRectMake(100, 400, 100, 100))
        
        sampleButton = UIButton(type: .Custom)
        
        // btnFBLogin.frame.height = 5
        //sampleButton!.frame = CGRect(x:50, y:500, width:70, height:50)
        // sampleButton!.frame = CGRect(x: 50, y: 500, width: 70, height: 50)
        
        
        //sampleButton =
        
        //sampleButton!.frame.size.height = 200
        sampleButton!.setTitle("Login with sendAsound", forState: .Normal)
        sampleButton!.titleLabel?.lineBreakMode = .ByWordWrapping
        sampleButton!.titleLabel?.textAlignment = .Center
        sampleButton!.setTitleColor(UIColor.blueColor(), forState: .Normal)
        // sampleButton!.layer.cornerRadius = 6
        sampleButton!.backgroundColor = UIColor.yellowColor().colorWithAlphaComponent(0.6)
        sampleButton?.tintColor =  UIColor.brownColor()
        
        
        //Add padding around text
        sampleButton!.titleEdgeInsets = UIEdgeInsetsMake(-10,-10,-10,-10)
        sampleButton!.contentEdgeInsets = UIEdgeInsetsMake(5,5,5,5)
        
        //Action set up
        sampleButton!.addTarget(self, action: #selector(StartVC.sampleButtonClicked), forControlEvents: .TouchUpInside)
        self.view.addSubview(sampleButton!)
        
        
        //Button Constraints:
        sampleButton!.translatesAutoresizingMaskIntoConstraints = false
        
        //To anchor above the tab bar on the bottom of the screen:
        let bottomButtonConstraint = sampleButton!.bottomAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor, constant: -150)
        
        // adjust height to be the same as facebook button
        let heightButtonConstraint = sampleButton!.heightAnchor.constraintEqualToConstant(btnFBLogin.frame.height)
        
        //edge of the screen in InterfaceBuilder:
        /*
         let margins = view.layoutMarginsGuide
         let leadingButtonConstraint = sampleButton!.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor, constant: 80)
         */
        
        sampleButton!.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor).active = true
        
        /*
         sampleButton.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor).active = true
         sampleButton.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
         */
        bottomButtonConstraint.active = true
        heightButtonConstraint.active = true
        // leadingButtonConstraint.active = true
    }
    
    func sampleButtonClicked(){
        
        print("sample Button Clicked")
        
        // let storyboard = UIStoryboard(name: "CreateAccountVC", bundle: nil)
        
        print("User: ", PFUser.currentUser()?.objectId)
        
        let installation = PFInstallation.currentInstallation()
        print("Installation: ", installation.objectId)
        
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("CreateAccountVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /* Cant be done, need password too.
     func sendAsoundLogin() {
     
     let userName = PFUser.currentUser()?.valueForKeyPath("first_name") as! String
     let passWord = PFUser.currentUser()?.valueForKeyPath("last_name") as! String
     
     // let user     = PFUser();
     
     //      self.fieldPassword.resignFirstResponder()
     //  self.fieldUserName.resignFirstResponder()
     
     PFUser.logInWithUsernameInBackground(userName , password: passWord , block: { (user, error) -> Void in
     
     if (user) != nil {
     
     // self.currentUser = user
     
     /*let alert = UIAlertController(title: "Success", message: "Logged in.", preferredStyle: UIAlertControllerStyle.Alert)
     
     alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:  { (action: UIAlertAction) -> Void in */
     
     let installation = PFInstallation.currentInstallation()
     
     print("logging in")
     installation["user"] = PFUser.currentUser()
     installation.saveInBackground()
     
     // self.goToTabBarCtrl()
     /*
     }))
     
     self.presentViewController(alert, animated: true, completion: nil)
     */
     } else {
     print(error)
     
     let alert = UIAlertController(title: "Login Failed", message: "Email or Password was incorrect.", preferredStyle: UIAlertControllerStyle.Alert)
     alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
     self.presentViewController(alert, animated: true, completion: nil)
     }
     
     })
     
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

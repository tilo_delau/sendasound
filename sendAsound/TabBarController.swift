//
//  TabBarController.swift
//  sendAsound
//
//  Created by Bang Means Do It on 04/03/2016.
//  Copyright © 2016 tilo. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.disableTabBarButtons(true)
    }
    
    func disableTabBarButtons(disable:Bool) {
        if disable {
            if  let arrayOfTabBarItems = self.tabBar.items {
                for item in arrayOfTabBarItems {
                    item.enabled = false
                }
                
                arrayOfTabBarItems[0].enabled = true
            }
            
        } else {
            if  let arrayOfTabBarItems = self.tabBar.items {
                for item in arrayOfTabBarItems {
                    item.enabled = true
                }
            }
        }
    }

}

